package com.atlassian.confluence.plugins.extractor;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.confluence.index.attachment.AttachmentTextExtractor;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.search.v2.extractor.util.AbstractLengthLimitedStringBuilder;
import com.atlassian.confluence.search.v2.extractor.util.LimitReachedException;
import com.atlassian.confluence.search.v2.extractor.util.StaticLengthLimitedStringBuilder;
import com.atlassian.confluence.search.v2.extractor.util.StringBuilderWriter;
import com.atlassian.confluence.util.io.InputStreamSource;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class PdfContentExtractor implements AttachmentTextExtractor {
    private static final Logger log = LoggerFactory.getLogger(PdfContentExtractor.class);

    private static final String[] EXTENSIONS = {"pdf"};
    private static final String[] CONTENT_TYPES = {"application/pdf"};
    private static final int DEFAULT_MAX_RESULT_SIZE = 8 * 1024 * 1024; // 8 MB
    private final int maxResultSize;
    private final AttachmentManager attachmentManager;

    public PdfContentExtractor(AttachmentManager attachmentManager) {
        this(attachmentManager, DEFAULT_MAX_RESULT_SIZE);
    }

    PdfContentExtractor(AttachmentManager attachmentManager, int maxResultSize) {
        this.attachmentManager = requireNonNull(attachmentManager);
        this.maxResultSize = maxResultSize;
    }

    @Override
    public List<String> getFileExtensions() {
        return Arrays.asList(EXTENSIONS);
    }

    @Override
    public List<String> getMimeTypes() {
        return Arrays.asList(CONTENT_TYPES);
    }

    @Override
    public Optional<InputStreamSource> extract(Attachment attachment) {
        return Optional.of(() -> {
            String text = "";
            try (InputStream is = attachmentManager.getAttachmentData(attachment)) {
                if (is == null) {
                    log.warn("Encountered attachment with null stream: " + attachment.getFileName());
                }
                log.debug("Starting to index attachment: " + attachment.getFileName());
                text = extractText(is);
            } catch (IOException e) {
                log.warn("Error reading attachment (" + attachment + ")", e);
            } catch (RuntimeException e) {
                log.warn("Error indexing attachment (" + attachment + ")", e);
            }
            return IOUtils.toInputStream(text, StandardCharsets.UTF_8);
        });
    }

    @VisibleForTesting
    String extractText(InputStream is) {
        try (PDDocument pdfDocument = PDDocument.load(is)) {
            PDFTextStripper stripper = new PDFTextStripper();
            StringBuilderWriter writer = new StringBuilderWriter(new StaticLengthLimitedStringBuilder(
                    maxResultSize / 2, // 1 char == 2 bytes
                    AbstractLengthLimitedStringBuilder.LIMIT_BEHAVIOUR.THROW)
            );
            try {
                stripper.writeText(pdfDocument, writer);
            } catch (LimitReachedException e) {
                // We got enough data
                log.debug("Reached maximum result length of {} bytes", maxResultSize);
            } finally {
                writer.close();
            }
            return writer.toString();
        } catch (InvalidPasswordException e) {
            //they didn't supply a password and the default of "" was wrong.
            throw new RuntimeException("Password required for encrypted PDF document", e);
        } catch (Exception e) {
            throw new RuntimeException("Error getting content of PDF document", e);
        }
    }
}
