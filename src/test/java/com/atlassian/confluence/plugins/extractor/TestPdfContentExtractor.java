package com.atlassian.confluence.plugins.extractor;

import com.atlassian.confluence.index.attachment.AttachmentTextExtractor;
import com.atlassian.confluence.internal.index.attachment.AttachmentExtractedTextHelper;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.doAnswer;

public class TestPdfContentExtractor {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private AttachmentManager attachmentManager;

    @Before
    public void setUp() {
        doAnswer(invocation -> {
            Object[] arguments = invocation.getArguments();
            Attachment attachment = (Attachment) arguments[0];
            return getResourceAsStream(attachment.getFileName());
        }).when(attachmentManager).getAttachmentData(ArgumentMatchers.any(Attachment.class));
    }

    @Test
    public void testSimplePdf() {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,2 * 1024 * 1024);
        Attachment attachment = createAttachment("test-attachment-search.pdf", "application/pdf");

        String text = extractText(extractor, attachment);

        assertThat(text, containsString("feature"));
        assertThat(text, not(containsString("apples")));
    }


    @Test
    public void testProtectedPdf() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Password required for encrypted PDF document");

        PdfContentExtractor extractor = new PdfContentExtractor(attachmentManager, 2 * 1024 * 1024);
        extractor.extractText(getResourceAsStream("protected.pdf"));
    }

    //BONNIE-43 - Testing that extraction works with PDFs with different PDF versions and content creators
    //Files are actual problem files that customers reported having issues with
    @Test
    public void testPdfWithDifferentContentCreators() {
        //PDF version 1.3
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,2 * 1024 * 1024);
        Attachment attachment = createAttachment("test-v1_3.pdf", "application/pdf");

        String text = extractText(extractor, attachment);
        assertThat(text, containsString("grasses"));
        assertThat(text, containsString("colleagues"));

        //PDF version 1.4
        attachment = createAttachment("test-v1_4.pdf", "application/pdf");

        text = extractText(extractor, attachment);
        assertThat(text, containsString("svn"));
    }

    @Test
    public void testInternationalisedPdf() {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,2 * 1024 * 1024);
        //Non-English characters
        Attachment attachment = createAttachment("chinese-characters.pdf", "application/pdf");
        String text = extractText(extractor, attachment);
        assertThat(text, containsString("\u5c0f\u96de"));


        //Right-to-left languages
        attachment = createAttachment("arabic-characters.pdf", "application/pdf");
        text = extractText(extractor, attachment);
        assertThat(text, containsString("Romanization"));
        assertThat(text, containsString("\u0637\u0648\u064a\u0644\u0629"));
    }

    @Test
    public void testExtractorWrongType() throws IOException {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,2 * 1024 * 1024);
        Attachment attachment = createAttachment("test-attachment-search.txt", "application/pdf");
        String text = extractText(extractor, attachment);
        assertThat(text, isEmptyString());
    }

    @Test
    public void testAllDropped() {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,128);
        Attachment attachment = createAttachment("test-v1_3.pdf", "application/pdf");

        String text = extractText(extractor, attachment);
        assertThat(text, not(containsString("grasses")));
        assertThat(text, not(containsString("colleagues")));
    }

    @Test
    public void testSomeDropped() {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,1024);
        Attachment attachment = createAttachment("test-v1_3.pdf", "application/pdf");

        String text = extractText(extractor, attachment);
        assertThat(text, containsString("grasses"));
        assertThat(text, not(containsString("colleagues")));
    }

    @Test
    public void testAllIncluded() {
        AttachmentTextExtractor extractor = new PdfContentExtractor(attachmentManager,8192);
        Attachment attachment = createAttachment("test-v1_3.pdf", "application/pdf");
        String text = extractText(extractor, attachment);
        assertThat(text, containsString("grasses"));
        assertThat(text, containsString("colleagues"));
    }

    private String extractText(AttachmentTextExtractor extractor, Attachment attachment) {
        return extractor.extract(attachment).flatMap(AttachmentExtractedTextHelper::toString).get();
    }

    private Attachment createAttachment(String fileName, String contentType) {
        return new Attachment(fileName, contentType, -1, "version 1");
    }

    private InputStream getResourceAsStream(String filename) {
        return getClass().getClassLoader().getResourceAsStream(filename);
    }
}
